package com.test.payway.repositories;

import com.test.payway.database.Libro;
import org.springframework.data.repository.CrudRepository;

public interface LibroRepository extends CrudRepository<Libro,Integer> {
}
