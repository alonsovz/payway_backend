package com.test.payway.repositories;

import com.test.payway.database.Autor;
import org.springframework.data.repository.CrudRepository;

public interface AutorRepository extends CrudRepository<Autor, Integer> {
}
