package com.test.payway.controllers;

import com.test.payway.database.Autor;
import com.test.payway.database.Categoria;
import com.test.payway.services.AutorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/test-api")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT})
public class AutorController {


    @Autowired
    AutorService autorService;

    @GetMapping("/autores")
    public List<Autor> obtenerAutores(){
        List<Autor> autores = this.autorService.findAll();
        return autores;
    }
}