package com.test.payway.controllers;

import com.test.payway.database.Libro;
import com.test.payway.services.LibroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/test-api")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT})

public class LibroController {

    @Autowired
    private LibroService libroService;

    @PutMapping("/obtener-libros/{id}")
    public Libro update(@PathVariable("id") Integer id, @RequestBody Libro libro){
        Libro l = this.libroService.update(libro,id);
        return l;
    }

    @GetMapping("/libros")
    public List<Libro> obtenerLibros(){
        List<Libro> libros = this.libroService.findAll();
        return libros;
    }

    @PostMapping("/libros")
    public Libro guardarLibro(@RequestBody Libro libro){
        Libro l = this.libroService.save(libro);
        return l;
    }






}
