package com.test.payway.controllers;

import com.test.payway.database.Categoria;
import com.test.payway.database.Libro;
import com.test.payway.services.CategoriaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/test-api")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT})
public class CategoriaController {

    @Autowired
    CategoriaService categoriaService;

    @GetMapping("/categorias")
    public List<Categoria> obtenerCategorias(){
        List<Categoria> cat = this.categoriaService.findAll();
        return cat;
    }
}
