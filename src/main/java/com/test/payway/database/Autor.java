package com.test.payway.database;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Entity
@Table(name="autores")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor

public class Autor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String nombre;
    private String pais;
    private Date fechaNacimiento;

    @OneToMany(mappedBy = "autor", cascade= CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonBackReference
    List<Libro> libros;

}
