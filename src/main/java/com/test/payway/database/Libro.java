package com.test.payway.database;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="libros")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class Libro {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String titulo;
    private Double precio;
    private Boolean estado;
    @ManyToOne
    @JoinColumn(name="categoria_id")
    private Categoria categoria;
    @ManyToOne
    @JoinColumn(name="autor_id")
    private Autor autor;







}

