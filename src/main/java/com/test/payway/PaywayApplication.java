package com.test.payway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaywayApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaywayApplication.class, args);
	}

}
