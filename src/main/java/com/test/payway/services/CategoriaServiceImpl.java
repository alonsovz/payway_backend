package com.test.payway.services;

import com.test.payway.database.Categoria;
import com.test.payway.repositories.CategoriaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoriaServiceImpl implements CategoriaService {

    @Autowired
    private CategoriaRepository categoriaRepository;

    @Override
    public List<Categoria> findAll() {
        List<Categoria> categorias = (List<Categoria>) this.categoriaRepository.findAll();
        return categorias;
    }
}
