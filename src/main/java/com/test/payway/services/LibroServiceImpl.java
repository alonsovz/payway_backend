package com.test.payway.services;

import com.test.payway.database.Libro;
import com.test.payway.repositories.LibroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LibroServiceImpl implements  LibroService {

    @Autowired
    private LibroRepository libroRepository;



    @Override
    public List<Libro> findAll() {
        List<Libro> libros = (List<Libro>) this.libroRepository.findAll();
        return libros;

    }

    @Override
    public Libro save(Libro libro) {
        return this.libroRepository.save(libro);
    }



    @Override
    public Libro update(Libro libro, Integer id) {
        Libro _librodto = this.libroRepository.findById(id).get();
        _librodto = libro;
        return this.libroRepository.save(_librodto);

    }
}
