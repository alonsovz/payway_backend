package com.test.payway.services;

import com.test.payway.database.Autor;

import java.util.List;

public interface AutorService {
    public List<Autor> findAll();
}
