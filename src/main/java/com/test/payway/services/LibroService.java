package com.test.payway.services;

import com.test.payway.database.Libro;

import java.util.List;

public interface LibroService {

    public List<Libro> findAll();
    public Libro save(Libro libro);

    public Libro update(Libro libro, Integer id);

}
