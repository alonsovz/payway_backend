package com.test.payway.services;

import com.test.payway.database.Categoria;

import java.util.List;

public interface CategoriaService {

    public List<Categoria> findAll();
}
