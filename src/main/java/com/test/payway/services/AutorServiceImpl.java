package com.test.payway.services;

import com.test.payway.database.Autor;
import com.test.payway.repositories.AutorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AutorServiceImpl implements AutorService {

    @Autowired
    private AutorRepository autorRepository;

    @Override
    public List<Autor> findAll() {
        List<Autor> autores = (List<Autor>) this.autorRepository.findAll();
        return autores;
    }
}
